## Reference-systems
The main purpose of this application is to provide a reference implementation of sender- and recipient-system
integration with Next Generation Digital Post (NgDP). It is intended to give insight into the process of sending and 
receiving MeMos and receipts to and from NgDP, by providing examples, which can assist respective implementers in their 
own integration to NgDP. 

The reference implementation is thus meant as a general guideline more so than a specific recipe for integration.

The reference implementation provides examples of system integration with NgDP for
the following protocols:
- REST_PUSH (sending and receiving)
- REST_PULL (receiving)
- SFTP (sending)


## Dependencies
This application is dependent on memo-lib-java, available here: https://bitbucket.org/nc-dp/memo-lib-java/src/master/

A range of libraries sourced from the Maven Central Repository is used to simplify the code, support functionality and 
reduce boilerplate. MavenCentral: https://search.maven.org/

## webinars
A webinar has been created to provide an overview of the recipient-systems and how they integrate to NgDP. Available
here: https://digst.dk/it-loesninger/naeste-generation-digital-post/for-myndigheder-og-it-leverandoerer/for-it-leverandoerer/referenceimplementeringer-og-memo-lib/

___

## Respository structure

### Overall
This application is separated into multiple sub-modules, each representing areas of responsibility. Sub-modules that 
include sending and receiving functionality are runnable as stand alone Spring Boot Applications. When run, these 
applications will trigger parts of the integration flow, and mimic much of the actual process for sending and receiving 
MeMos and Business Receipts for the given protocol. 

As an example, running the ReferenceSystemRestPushApplication in the system-rest-push sub-module:
```Java 
@SpringBootApplication
public class ReferenceSystemRestPushApplication {
  public static void main(String[] args) {
    SpringApplication.run(ReferenceSystemRestPushApplication.class, args);
  }
}
```
Will showcase the creation of a MeMo and how this is attached to a REST request and sent to the correct endpoint in 
NgDP. It will also enable the reference REST_PUSH recipient-system to receive MeMos from NgDP and create and send back 
Business Receipts upon receiving these.

### System sub-modules
#### system-rest-push
Reference implementation of REST_PUSH protocol sender- and recipient-system. Showcases how REST requests to NgDP can be 
implemented, utilizing the RestClient provided by ssl-client - And how the recipient-system can provide an endpoint for
receiving MeMos from NgDP - as well as the processing of these received MeMos and the creation and sending of Business 
Receipts back to NgDP.

#### system-rest-publish-subscribe
Reference implementation of REST_PUBLISH_SUBSCRIBE protocol recipient-system. Showcases how REST calls can be made to fetch 
information about MeMos currently available for fetching, as well as fetching these one by one from NgDP. Creates and 
sends back Business Receipts to NgDP.

#### system-sftp
Reference implementation of SFTP protocol sender-system. Showcases how created MeMos can be bundled to a zip file and 
uploaded to NgDP's SFTP server - as well as downloading available receipts from the SFTP server. 

### Utility sub-modules
These sub-modules generally provide logic and utility not specific to any one protocol.

#### ssl-client
Implements mutual SSL authentication in the communication between the reference systems and NgDP. 

The certificate used for egress requests from sender-system to NgDP is stored in 
\system-{protocol}\src\main\resources\sender-system-keystore. 

The certificate used for ingress requests from NgDP to recipient-system is stored in 
\system-{protocol}\src\main\resources\recipient-system-keystore.
 
The trust-store used to validate ingress requests to the recipient-system is stored in 
\system-rest-push\src\main\resources\recipient-system-truststore.

#### utility-library
Provides model and service classes for MeMos and Business Receipts, handling the creation, parsing and logging of MeMos,
as well as the creation, sending and logging of positive or negative Business Receipts.
- MeMoService: creating MeMos.
- MeMoPersistenceService: saving MeMo as XML and creating it as a file.
- MeMoLoggingService: logging elements of MeMo messageHeader to console.
- MeMoParsingService: parsing MessageHeader element of MeMo.
- BusinessReceiptFactory: creating positive or negative Business Receipts.
- ReceivedBusinessReceiptLoggingService: logging received Business Receipts to console.
- SentBusinessReceiptLoggingService: logging responses from sent Business Receipts to console.
---

