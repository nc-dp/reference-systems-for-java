package dk.digst.digital.post.reference.systems.restpush;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties
@ComponentScan(basePackages = "dk.digst.digital.post.reference.systems")
public class ReferenceSystemsRestPushConfiguration {}