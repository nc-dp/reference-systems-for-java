package dk.digst.digital.post.reference.systems.restpush.recipient.api;

import dk.digst.digital.post.memolib.parser.MeMoParseException;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import java.io.IOException;
import java.util.UUID;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@RequestMapping(value = "recipient-system")
public interface RecipientSystemMemoEndpointApi {

  @PostMapping(
      value = "/",
      consumes = {MediaType.APPLICATION_XML_VALUE},
      produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
  @Operation(
      description = "Receives individual xml MeMos",
      responses = {
        @ApiResponse(responseCode = "200", description = "OK"),
        @ApiResponse(responseCode = "400", description = "Bad request"),
        @ApiResponse(responseCode = "401", description = "Unauthorized"),
        @ApiResponse(responseCode = "403", description = "Forbidden"),
        @ApiResponse(responseCode = "404", description = "Not found"),
        @ApiResponse(responseCode = "503", description = "Service unavailable"),
      })
  @Parameter(
      in = ParameterIn.QUERY,
      description = "messageUUID of the received MeMo.",
      name = "memo-message-uuid",
      content = @Content(schema = @Schema(type = "string", format = "uuid")))
  ResponseEntity<String> recipientSystemMemoEndpoint(
      @RequestParam(value = "memo-message-uuid") UUID memoMessageUuid, @RequestBody Resource file)
      throws MeMoParseException, IOException;
}
