package dk.digst.digital.post.reference.systems.restpush;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ReferenceSystemsRestPushApplication {

  public static void main(String[] args) {
    SpringApplication.run(ReferenceSystemsRestPushApplication.class, args);
  }
}
