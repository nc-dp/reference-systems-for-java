package dk.digst.digital.post.reference.systems.restpush.sender.controllers;

import dk.digst.digital.post.reference.systems.restpush.sender.api.SenderSystemEndpointApi;
import dk.digst.digital.post.reference.systems.utility.library.receipt.model.Receipt;
import dk.digst.digital.post.reference.systems.utility.library.receipt.services.ReceivedBusinessReceiptLoggingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SenderSystemEndpointController implements SenderSystemEndpointApi {

  @Autowired private ReceivedBusinessReceiptLoggingService receivedBusinessReceiptLoggingService;

  /**
   * REST-method that simulates the receiptEndpoint of a Sender System configured through
   * Administrative access.
   *
   * <p>When a MeMo is sent from a Sender System, it is validated and processed by NgDP. NgDP will
   * send a Business Receipt back to the Sender System, notifying whether this process was
   * successful or unsuccessful, reflected through the ReceiptStatus.
   *
   * @param businessReceipt - the Business Receipt sent from NgDP.
   * @return HttpStatus to NgDP.
   */
  @Override
  public ResponseEntity<Void> receipt(Receipt businessReceipt) {
    receivedBusinessReceiptLoggingService.handleReceivedBusinessReceipt(businessReceipt);
    return new ResponseEntity<>(HttpStatus.OK);
  }
}
