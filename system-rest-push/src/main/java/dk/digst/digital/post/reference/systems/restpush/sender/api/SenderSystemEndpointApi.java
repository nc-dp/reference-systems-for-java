package dk.digst.digital.post.reference.systems.restpush.sender.api;

import dk.digst.digital.post.reference.systems.utility.library.receipt.model.Receipt;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.IOException;

@RequestMapping(value = "sender-system")
public interface SenderSystemEndpointApi {

  @PostMapping(
      value = "/receipt",
      consumes = {MediaType.APPLICATION_JSON_VALUE},
      produces = {MediaType.APPLICATION_JSON_VALUE})
  @Operation(
      description = "Accepts a business receipt for a single MeMo",
      responses = {
        @ApiResponse(responseCode = "201", description = "Created"),
        @ApiResponse(responseCode = "400", description = "Bad request"),
        @ApiResponse(responseCode = "401", description = "Unauthorized"),
        @ApiResponse(responseCode = "403", description = "Forbidden"),
        @ApiResponse(responseCode = "503", description = "Service unavailable"),
      })
  ResponseEntity<Void> receipt(@Validated @RequestBody Receipt businessReceipt) throws IOException;
}
