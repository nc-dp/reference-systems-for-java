package dk.digst.digital.post.reference.systems.restpush.sender.services;

import dk.digst.digital.post.reference.systems.restpush.sender.configuration.SystemConfigurationProperties;
import dk.digst.digital.post.reference.systems.ssl.client.clients.RestClient;
import dk.digst.digital.post.reference.systems.utility.library.memo.services.MeMoService;
import dk.digst.digital.post.reference.systems.utility.library.receipt.model.Receipt;
import java.io.File;
import java.util.UUID;
import javax.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class MeMoPushService {

  @Autowired private MeMoService meMoService;

  @Autowired private SystemConfigurationProperties systemConfigurationProperties;

  @Autowired private RestClient restClient;

  /**
   * Builds a MeMo, saves it to local file path, and sends it to NgDP as single .XML
   */
  @PostConstruct
  public void sendMeMo() {
    UUID memoUuid = UUID.randomUUID();
    File meMoFile = meMoService.createMemoXml(memoUuid);
    log.info("Sending MeMo to NgDP");
    sendMemoXmlRequest(meMoFile, memoUuid);
  }

  /**
   * Builds a .TAR archive with three MeMos, and sends it to NgDP
   */
  @PostConstruct
  public void sendMemoArchive() {
    File memoTar = meMoService.createMemoArchive();
    log.info("Sending MeMo tar to NgDP");
    sendMemoArchiveRequest(memoTar);
  }

  private void sendMemoXmlRequest(File file, UUID memoUuid) {
    try {
      ResponseEntity<Receipt> response =
              restClient.sendRequest(
                      systemConfigurationProperties.getNgdpEndpoint() + "?memo-message-uuid=" + memoUuid,
                      new FileSystemResource(file),
                      Receipt.class);

      log.info(
              "Request sent to NgDP. TransmissionId: {}. Timestamp: {}",
              response.getBody().getTransmissionId(),
              response.getBody().getTimeStamp());

    } catch (Exception e) {
      log.error("Unable to send XML to NgDP. Exception: {}", e.toString());
    }
  }

  private void sendMemoArchiveRequest(File file) {
    try {
      ResponseEntity<Receipt> response =
          restClient.sendRequest(
              systemConfigurationProperties.getNgdpEndpoint(),
              new FileSystemResource(file),
              Receipt.class);

        log.info(
            "Request sent to NgDP. TransmissionId: {}. Timestamp: {}",
            response.getBody().getTransmissionId(),
            response.getBody().getTimeStamp());

    } catch (Exception e) {
      log.error("Unable to send TAR to NgDP. Exception: {}", e.toString());
    }
  }
}
