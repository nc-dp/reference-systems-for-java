package dk.digst.digital.post.reference.systems.restpush.recipient.controllers;

import dk.digst.digital.post.memolib.model.MessageHeader;
import dk.digst.digital.post.memolib.parser.MeMoParseException;
import dk.digst.digital.post.reference.systems.restpush.recipient.api.RecipientSystemMemoEndpointApi;
import dk.digst.digital.post.reference.systems.restpush.sender.configuration.SystemConfigurationProperties;
import dk.digst.digital.post.reference.systems.ssl.client.clients.RestClient;
import dk.digst.digital.post.reference.systems.utility.library.memo.services.MeMoLoggingService;
import dk.digst.digital.post.reference.systems.utility.library.memo.services.MeMoParsingService;
import dk.digst.digital.post.reference.systems.utility.library.receipt.model.Receipt;
import dk.digst.digital.post.reference.systems.utility.library.receipt.services.BusinessReceiptFactory;
import dk.digst.digital.post.reference.systems.utility.library.receipt.services.SentBusinessReceiptLoggingService;
import java.io.IOException;
import java.util.UUID;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;

@Slf4j
@RestController
public class RecipientSystemMemoEndpointController implements RecipientSystemMemoEndpointApi {

  @Autowired private MeMoParsingService meMoParsingService;

  @Autowired private SentBusinessReceiptLoggingService sentBusinessReceiptLoggingService;

  @Autowired private BusinessReceiptFactory businessReceiptFactory;

  @Autowired private MeMoLoggingService memoLoggingService;

  @Autowired private RestClient restClient;

  @Autowired private SystemConfigurationProperties systemConfigurationProperties;

  /**
   * REST-method that simulates the endpoint of a REST_PUSH recipient-system configured through
   * Administrative access. When NgDP has validated a MeMo, it will look up the protocol of the
   * recipient-system, and in case of REST_PUSH, immediately send the MeMo to the endPoint field of
   * the recipient-system.
   *
   * @param memoMessageUuid - the uuid of the received memo.
   * @param memoXml - the validated MeMo sent to the recipient-system Endpoint.
   * @return UUID of specific MeMo as String.
   */
  @Override
  public ResponseEntity<String> recipientSystemMemoEndpoint(UUID memoMessageUuid, Resource memoXml)
      throws IOException {

    String memoFileName = memoXml.getFilename();
    MessageHeader memoMessageHeader = new MessageHeader();
    Receipt receipt;

    try {
      // Assess the validity of the MeMo MessageHeader and produce Business Receipt.
      memoMessageHeader = meMoParsingService.parseMemo(memoXml.getInputStream());
      receipt = businessReceiptFactory.createPositiveBusinessReceipt(memoMessageHeader);
      memoLoggingService.logMemoSuccessfullyParsed(receipt, memoMessageHeader);
    } catch (MeMoParseException e) {
      receipt = businessReceiptFactory.createNegativeBusinessReceipt(memoFileName);
      memoLoggingService.logMemoUnsuccessfullyParsed(receipt, memoFileName);
    }

    final String ngdpRestPostReceiptEndpoint =
        String.format(
            systemConfigurationProperties.getNgdpReceiptEndpoint(), memoXml.getFilename());

    ResponseEntity<String> response = null;
    try {
      response = restClient.post(ngdpRestPostReceiptEndpoint, receipt, String.class);
      sentBusinessReceiptLoggingService.logSentReceiptResponse(
          receipt, response, memoXml.getFilename());
    } catch (HttpClientErrorException e) {
      log.error("Resource with id: {} not found, error: {}.", memoFileName, e.getMessage());
    }

    return new ResponseEntity<>(memoMessageHeader.getMessageUUID().toString(), HttpStatus.OK);
  }
}
