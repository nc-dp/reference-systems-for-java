package dk.digst.digital.post.reference.systems.restpush

import dk.digst.digital.post.memolib.container.ArchiveEntryIterator
import dk.digst.digital.post.memolib.container.ArchiveEntryIteratorFactory
import dk.digst.digital.post.memolib.container.MeMoContainerReader
import dk.digst.digital.post.memolib.model.Message
import dk.digst.digital.post.memolib.model.MessageType
import dk.digst.digital.post.memolib.parser.MeMoParser
import dk.digst.digital.post.memolib.parser.MeMoParserFactory
import dk.digst.digital.post.reference.systems.utility.library.receipt.model.Receipt
import org.springframework.core.io.Resource
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity

class SendMeMoSpec extends AbstractSpecification {

  void "Memo is built correctly and sent to the correct endpoint"() {
    given:
    Resource resource

    when: "A message is sent"
    meMoPushService.sendMeMo()

    then: "A correctly formed memo is sent to NgDP"
    1 * restClient.sendRequest(
            _,
            _,
            Receipt.class
    ) >> {
      arguments ->
        resource = (Resource) arguments[1];
        return new ResponseEntity<Receipt>(new Receipt(), HttpStatus.OK)
    }

    resource != null
    MeMoParser parser = MeMoParserFactory.createParser(resource.inputStream, true)
    Message message = parser.parse()

    with(message.messageHeader) {
      messageType == MessageType.DIGITALPOST
    }

    and: "nothing unexpected is called"
    0 * _
  }

  void "Memo tar is built correctly and sent to the correct endpoint"() {
    given:
    Resource resource

    when: "The receiver receives the metadata"
    meMoPushService.sendMemoArchive()

    then: "A correctly formed memo is sent to digital post"
    1 * restClient.sendRequest(
            systemConfigurationProperties.ngdpEndpoint,
            _,
            Receipt.class
    ) >> {
      arguments ->
        resource = (Resource) arguments[1];
        return new ResponseEntity<Receipt>(new Receipt(), HttpStatus.OK)
    }

    resource != null
    ArchiveEntryIterator archiveEntryIterator = new ArchiveEntryIteratorFactory().newArchiveEntryIterator(resource.inputStream)
    MeMoContainerReader reader = new MeMoContainerReader(archiveEntryIterator)

    Message message1 = reader.readEntry().get()
    Message message2 = reader.readEntry().get()
    Message message3 = reader.readEntry().get()

    with(message1.messageHeader) {
      messageType == MessageType.DIGITALPOST
    }

    with(message2.messageHeader) {
      messageType == MessageType.DIGITALPOST
    }

    with(message3.messageHeader) {
      messageType == MessageType.DIGITALPOST
    }

    reader.readEntry().isEmpty()
    reader.close()
    archiveEntryIterator.close()

    and: "nothing unexpected is called"
    0 * _
  }

}
