package dk.digst.digital.post.reference.systems.restpush

import dk.digst.digital.post.reference.systems.utility.library.receipt.model.Receipt
import dk.digst.digital.post.reference.systems.utility.library.receipt.model.ReceiptStatus

import java.time.LocalDateTime;

class ReceiveReceiptSpec extends AbstractSpecification {

    void "Receipts are handled when received"(){
        given: "A receipt"
        Receipt receipt = new Receipt(
                receiptStatus: ReceiptStatus.COMPLETED,
                transmissionId: UUID.randomUUID(),
                timeStamp: LocalDateTime.now()
        )

        when: "the receipt is sent to this component"
        senderSystemEndpointController.receipt(receipt)

        then: "the receipt is handled"
        1 * receivedBusinessReceiptLoggingService.handleReceivedBusinessReceipt(receipt)
        0 * _
    }
}
