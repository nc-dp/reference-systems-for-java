package dk.digst.digital.post.reference.systems.restpush

import dk.digst.digital.post.reference.systems.restpush.sender.configuration.SystemConfigurationProperties
import dk.digst.digital.post.reference.systems.restpush.sender.controllers.SenderSystemEndpointController
import dk.digst.digital.post.reference.systems.restpush.sender.services.MeMoPushService
import dk.digst.digital.post.reference.systems.ssl.client.clients.RestClient
import dk.digst.digital.post.reference.systems.utility.library.memo.services.MeMoPersistenceService
import dk.digst.digital.post.reference.systems.utility.library.memo.services.MeMoService
import dk.digst.digital.post.reference.systems.utility.library.receipt.services.ReceivedBusinessReceiptLoggingService
import spock.lang.Specification

abstract class AbstractSpecification extends Specification {
  ReceivedBusinessReceiptLoggingService receivedBusinessReceiptLoggingService = Mock(ReceivedBusinessReceiptLoggingService)

  SenderSystemEndpointController senderSystemEndpointController

  RestClient restClient = Mock(RestClient)

  SystemConfigurationProperties systemConfigurationProperties

  MeMoPushService meMoPushService

  void setup() {
    systemConfigurationProperties = new SystemConfigurationProperties(
            ngdpEndpoint: "https://digitalpost.dk/memos",
    )
    MeMoPersistenceService meMoPersistenceService = new MeMoPersistenceService()
    MeMoService meMoService = new MeMoService(meMoPersistenceService: meMoPersistenceService)

    meMoPushService = new MeMoPushService(
            systemConfigurationProperties: systemConfigurationProperties,
            meMoService: meMoService,
            restClient: restClient
    )

    senderSystemEndpointController = new SenderSystemEndpointController(
            receivedBusinessReceiptLoggingService: receivedBusinessReceiptLoggingService
    )
  }
}
