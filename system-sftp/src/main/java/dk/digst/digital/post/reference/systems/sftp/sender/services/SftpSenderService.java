package dk.digst.digital.post.reference.systems.sftp.sender.services;

import dk.digst.digital.post.reference.systems.utility.library.memo.services.MeMoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.file.support.FileExistsMode;
import org.springframework.integration.sftp.session.SftpRemoteFileTemplate;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.File;

@Slf4j
@Service
public class SftpSenderService {

  @Autowired private MeMoService meMoService;

  @Autowired private SftpRemoteFileTemplate sftpRemoteFileTemplate;

  /** Constructs a tar file with three memos and uploads it to NgDP's SFTP server */
  @PostConstruct
  public void sendMemoTar() {
    File zipFile = meMoService.createMemoArchive();

    log.info("Uploading file with name {}", zipFile.getName());
    sftpRemoteFileTemplate.send(
        MessageBuilder.withPayload(zipFile).setHeader("file_name", zipFile.getName()).build(),
        FileExistsMode.REPLACE);
  }
}
