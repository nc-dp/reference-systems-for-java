package dk.digst.digital.post.reference.systems.sftp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ReferenceSystemsSftpApplication {

  public static void main(String[] args) {
    SpringApplication.run(ReferenceSystemsSftpApplication.class, args);
  }
}
