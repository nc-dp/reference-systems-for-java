package dk.digst.digital.post.reference.systems.sftp.sender.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

@Getter
@Setter
@Component
@ConfigurationProperties(prefix = "dk.digst.digital.post.referencesystems.sftp")
public class SftpSystemConfigurationProperties {
  private String sshUsername;
  private String host;
  private String memoFolder;
  private String receiptFolder;
  private String temporaryMemoFolder;
  private int port;
  private Resource privateKeyUri;
  private String privateKeyPassword;
  private String downloadPath;
}
