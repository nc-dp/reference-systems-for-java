package dk.digst.digital.post.reference.systems.sftp.sender.handlers;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.sftp.session.SftpRemoteFileTemplate;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHandler;
import org.springframework.stereotype.Service;

import java.io.File;
import java.nio.file.Files;

@Slf4j
@Service
public class SftpReceiptMessageHandler implements MessageHandler {

  /**
   * Handles new receipts fetched from the SFTP server
   *
   * @param message Generic message about new receipt fetched from SFTP server
   */
  @SneakyThrows
  @Override
  public void handleMessage(Message<?> message) {
    File receiptFile = (File) message.getPayload();
    log.info("Received receipt: {}", Files.readString(receiptFile.toPath()));

    Files.delete((receiptFile).toPath());
  }
}
