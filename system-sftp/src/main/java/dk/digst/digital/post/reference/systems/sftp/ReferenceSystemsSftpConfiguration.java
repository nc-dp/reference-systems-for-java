package dk.digst.digital.post.reference.systems.sftp;

import com.jcraft.jsch.ChannelSftp;
import dk.digst.digital.post.reference.systems.sftp.sender.config.SftpSystemConfigurationProperties;
import dk.digst.digital.post.reference.systems.sftp.sender.handlers.SftpReceiptMessageHandler;
import java.io.File;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.expression.common.LiteralExpression;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.dsl.Pollers;
import org.springframework.integration.expression.FunctionExpression;
import org.springframework.integration.file.remote.session.CachingSessionFactory;
import org.springframework.integration.file.remote.session.SessionFactory;
import org.springframework.integration.scheduling.PollerMetadata;
import org.springframework.integration.sftp.dsl.Sftp;
import org.springframework.integration.sftp.session.DefaultSftpSessionFactory;
import org.springframework.integration.sftp.session.SftpRemoteFileTemplate;
import org.springframework.messaging.Message;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.support.CronTrigger;

@Configuration
@EnableScheduling
@EnableConfigurationProperties
@ComponentScan(basePackages = "dk.digst.digital.post.reference.systems")
public class ReferenceSystemsSftpConfiguration {

  @Autowired SftpSystemConfigurationProperties sftpSystemConfigurationProperties;

  /**
   * Setup spring integration session factory
   *
   * @return Factory with SFTP server setup
   */
  @Bean
  public SessionFactory<ChannelSftp.LsEntry> sftpSessionFactory() {
    DefaultSftpSessionFactory factory = new DefaultSftpSessionFactory(true);
    factory.setHost(sftpSystemConfigurationProperties.getHost());
    factory.setPort(sftpSystemConfigurationProperties.getPort());
    factory.setUser(sftpSystemConfigurationProperties.getSshUsername());
    factory.setPrivateKey(sftpSystemConfigurationProperties.getPrivateKeyUri());
    factory.setPrivateKeyPassphrase(sftpSystemConfigurationProperties.getPrivateKeyPassword());
    factory.setAllowUnknownKeys(true);
    return new CachingSessionFactory<>(factory);
  }

  /**
   * Setup spring integration SftpRemoteFileTemplate used when uploading MeMo zip files
   *
   * @param sftpSessionFactory Factory containing SFTP server setup
   * @return SftpRemoteFileTemplate
   */
  @Bean
  public SftpRemoteFileTemplate sftpRemoteFileTemplate(
      SessionFactory<ChannelSftp.LsEntry> sftpSessionFactory) {
    final SftpRemoteFileTemplate template = new SftpRemoteFileTemplate(sftpSessionFactory);

    /* Set remote directory to upload MeMo to */
    template.setRemoteDirectoryExpression(
        new LiteralExpression(sftpSystemConfigurationProperties.getMemoFolder()));

    /* Set temporary directory to use while uploading MeMo */
    template.setTemporaryRemoteDirectoryExpression(
        new FunctionExpression<Message<?>>(
            message -> sftpSystemConfigurationProperties.getTemporaryMemoFolder()));

    return template;
  }

  /**
   * Setup of poller that polls every second
   *
   * @return Poller metadata
   */
  @Bean(name = PollerMetadata.DEFAULT_POLLER)
  public PollerMetadata defaultPoller() {
    return Pollers.trigger(new CronTrigger("* * * * * *")).get();
  }

  /**
   * Setup spring integration IntegrationFlow used to fetch receipts from SFTP server
   *
   * @param sftpSessionFactory Factory containing SFTP server setup
   * @param sftpReceiptMessageHandler Class to handle the fetched receipts
   * @param defaultPoller Scheduler for polling
   * @return IntegrationFlow that handles receipt fetching
   */
  @Bean
  public IntegrationFlow fetchReceiptsFlow(
      SessionFactory<ChannelSftp.LsEntry> sftpSessionFactory,
      SftpReceiptMessageHandler sftpReceiptMessageHandler,
      PollerMetadata defaultPoller) {

    return IntegrationFlows.from(
            Sftp.inboundAdapter(sftpSessionFactory)
                .remoteDirectory(sftpSystemConfigurationProperties.getReceiptFolder())
                .deleteRemoteFiles(true)
                .filterFunction(
                    entry ->
                        !entry.getAttrs().isDir()
                            && entry.getFilename() != null
                            && entry.getFilename().endsWith(".xml"))
                .localDirectory(new File(sftpSystemConfigurationProperties.getDownloadPath())),
            e -> e.autoStartup(true).poller(defaultPoller))
        .handle(sftpReceiptMessageHandler)
        .get();
  }
}
