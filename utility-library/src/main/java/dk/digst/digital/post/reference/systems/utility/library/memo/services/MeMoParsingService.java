package dk.digst.digital.post.reference.systems.utility.library.memo.services;

import dk.digst.digital.post.memolib.model.MessageHeader;
import dk.digst.digital.post.memolib.parser.MeMoParseException;
import dk.digst.digital.post.memolib.parser.MeMoParser;
import dk.digst.digital.post.memolib.parser.MeMoParserFactory;
import java.io.IOException;
import java.io.InputStream;
import org.springframework.stereotype.Service;

@Service
public class MeMoParsingService {

  /**
   * Implements a messageHeaderVisitor from memo-lib to parse the MessageHeader of the MeMo.
   *
   * @param memoInputStream - the received MeMo file as InputStream.
   * @return A MeMo MessageHeader object.
   */
  public MessageHeader parseMemo(InputStream memoInputStream)
      throws MeMoParseException, IOException {
    MeMoParser parser = MeMoParserFactory.createParser(memoInputStream, true);
    return parser.parse().getMessageHeader();
  }
}
