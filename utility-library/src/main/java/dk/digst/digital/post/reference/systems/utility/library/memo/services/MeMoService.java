package dk.digst.digital.post.reference.systems.utility.library.memo.services;

import dk.digst.digital.post.memolib.builder.FileBuilder;
import dk.digst.digital.post.memolib.builder.FileContentBuilder;
import dk.digst.digital.post.memolib.builder.MainDocumentBuilder;
import dk.digst.digital.post.memolib.builder.MessageBodyBuilder;
import dk.digst.digital.post.memolib.builder.MessageBuilder;
import dk.digst.digital.post.memolib.builder.MessageHeaderBuilder;
import dk.digst.digital.post.memolib.builder.RecipientBuilder;
import dk.digst.digital.post.memolib.builder.SenderBuilder;
import dk.digst.digital.post.memolib.container.MeMoContainerWriter;
import dk.digst.digital.post.memolib.container.MeMoContainerWriterFactory;
import dk.digst.digital.post.memolib.model.Message;
import dk.digst.digital.post.memolib.model.MessageBody;
import dk.digst.digital.post.memolib.model.MessageType;
import dk.digst.digital.post.memolib.model.Recipient;
import dk.digst.digital.post.memolib.model.Sender;
import java.io.File;
import java.io.FileOutputStream;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.UUID;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MeMoService {

  @Autowired private MeMoPersistenceService meMoPersistenceService;

  /**
   * Builds a memo as File
   *
   * @return MeMo file
   */
  public File createMemoXml(UUID memoUuid) {
    Message message = buildMeMo(memoUuid);
    return meMoPersistenceService.saveMemo(message);
  }

  /**
   * Builds a tar file with 3 memos
   *
   * @return the tar file
   */
  @SneakyThrows
  public File createMemoArchive() {
    File file = meMoPersistenceService.createNewFile(UUID.randomUUID() + ".tar.lzma");

    try (MeMoContainerWriter writer = new MeMoContainerWriterFactory().newMeMoContainerWriter(new FileOutputStream(file))) {
      for (int i = 0; i < 3; i++) {
        Message message = buildMeMo(UUID.randomUUID());
        meMoPersistenceService.writeTarEntry(writer, message);
      }
    }

    return file;
  }

  /**
   * Builds a memo as Message
   *
   * @return MeMo message
   */
  public Message createMeMo() {
    Message message = buildMeMo(UUID.randomUUID());
    meMoPersistenceService.saveMemo(message);
    return message;
  }

  private Message buildMeMo(UUID memoUuid) {
    return MessageBuilder.newBuilder()
        .messageHeader(
            MessageHeaderBuilder.newBuilder()
                .messageType(MessageType.DIGITALPOST)
                .messageUUID(memoUuid)
                .sender(buildSender())
                .recipient(buildRecipient())
                .notification("advis tekst")
                .label("titel på meddelelse")
                .mandatory(false)
                .legalNotification(false)
                .build())
        .messageBody(buildMessageBody())
        .build();
  }

  private MessageBody buildMessageBody() {
    return MessageBodyBuilder.newBuilder()
        .createdDateTime(ZonedDateTime.now(ZoneOffset.UTC).toLocalDateTime())
        .mainDocument(
            MainDocumentBuilder.newBuilder()
                .mainDocumentId("123")
                .label("Hoveddokument")
                .addFile(
                    FileBuilder.newBuilder()
                        .inLanguage("da")
                        .encodingFormat("text/plain")
                        .filename("hovedfil.txt")
                        .content(
                            FileContentBuilder.newBuilder()
                                .base64Content(
                                    "RGVyIGVyIGluZ2VuIGdydW5kIHRpbCBhdCBsw6ZzZSBkZXQgaGVyLCBkZXIgc3TDpXIgaWtrZSBub2dldCBpbnRlcmVzc2FudA==")
                                .build())
                        .build())
                .build())
        .build();
  }

  private Sender buildSender() {
    return SenderBuilder.newBuilder()
        .senderId("11221122")
        .idType("cvr")
        .label("afsendernavn")
        .build();
  }

  private Recipient buildRecipient() {
    return RecipientBuilder.newBuilder()
        .recipientId("0000000000")
        .idType("cpr")
        .build();
  }
}
