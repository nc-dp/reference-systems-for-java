package dk.digst.digital.post.reference.systems.utility.library.receipt.services;

import dk.digst.digital.post.reference.systems.utility.library.receipt.model.Receipt;
import dk.digst.digital.post.reference.systems.utility.library.receipt.model.ReceiptStatus;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class ReceivedBusinessReceiptLoggingService {

  /**
   * Checks the ReceiptStatus on the Business Receipt received by NgDP and logs relevant fields to
   * console.
   *
   * <p>The ReceiptStatus is the primary gauge of success of the validation and processing of the
   * MeMo sent by the Sender System. In case something went wrong in NgDP's attempt to process and
   * deliver the MeMo, and the ReceiptStatus is thus not ReceiptStatus.COMPLETED, the errorCode and
   * errorMessage fields will provide further details.
   *
   * <p>Example of a Negative Business Receipt:
   *
   * <p>{
   *
   * <p>"transmissionId":"2e03cfb8-fdce-4ab8-9f26-98e039da7c9d",
   *
   * <p>"messageUUID":"bf9fcafc-db8f-4765-bb46-a4a450368ce7",
   *
   * <p>"errorCode":"recipient.is.exempt",
   *
   * <p>"errorMessage":"Recipient with cvr 22222222 is exempt",
   *
   * <p>"timeStamp":"2020-05-19T12:41:06.723738", "receiptStatus":"NOT_ALLOWED"
   *
   * <p>}
   *
   * <p>In the above example, the Sender System has attempted to send a MeMo to a cvr that is exempt
   * from receiving Digital Post, and NgDP was thus not able to process it properly. The MeMo is
   * thus still the responsibility of the Sender System.
   *
   * @param businessReceipt - The Business Receipt received by NgDP.
   */
  public void handleReceivedBusinessReceipt(Receipt businessReceipt) {

    if (businessReceipt.getReceiptStatus() == ReceiptStatus.COMPLETED) {
      log.info(
          "Positive Business Receipt received from NgDP for MeMo with uuid: {} \nstatus: {}, \ntimestamp {}",
          businessReceipt.getMessageUUID(),
          businessReceipt.getReceiptStatus(),
          businessReceipt.getTimeStamp());

    } else {
      log.info(
          "Negative Business Receipt received from NgDP for MeMo with uuid: {} \nstatus: {}, \nerrorMessage: {}, \nerrorCode: {},  \ntimestamp {}",
          businessReceipt.getMessageUUID(),
          businessReceipt.getReceiptStatus(),
          businessReceipt.getErrorMessage(),
          businessReceipt.getErrorCode(),
          businessReceipt.getTimeStamp());
    }
    log.info(ToStringBuilder.reflectionToString(businessReceipt, ToStringStyle.MULTI_LINE_STYLE));
  }
}
