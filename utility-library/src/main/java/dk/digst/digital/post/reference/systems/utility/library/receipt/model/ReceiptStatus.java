package dk.digst.digital.post.reference.systems.utility.library.receipt.model;

public enum ReceiptStatus {
  COMPLETED,
  NOT_ALLOWED,
  INVALID,
  RECEIVED
}
