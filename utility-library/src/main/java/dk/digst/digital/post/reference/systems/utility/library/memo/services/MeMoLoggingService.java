package dk.digst.digital.post.reference.systems.utility.library.memo.services;

import dk.digst.digital.post.memolib.model.MessageHeader;
import dk.digst.digital.post.reference.systems.utility.library.receipt.model.Receipt;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class MeMoLoggingService {

  public void logMemoSuccessfullyParsed(Receipt businessReceipt, MessageHeader memoMessageHeader) {
    log.info(
        "Memo received and parsed with id: {}, from sender: {}, Positive Business Receipt created with timestamp: {}",
        memoMessageHeader.getMessageUUID(),
        memoMessageHeader.getSender().getLabel(),
        businessReceipt.getTimeStamp());
    log.info(ToStringBuilder.reflectionToString(memoMessageHeader, ToStringStyle.MULTI_LINE_STYLE));

    log.info(
        "Positive Business Receipt created as response to Memo with id: {}, with timestamp: {}",
        memoMessageHeader.getMessageUUID(),
        businessReceipt.getTimeStamp());
    log.info(ToStringBuilder.reflectionToString(businessReceipt, ToStringStyle.MULTI_LINE_STYLE));
  }

  public void logMemoUnsuccessfullyParsed(Receipt businessReceipt, String memoFileName) {
    log.info(
        "Unable to parse MeMo with id: {}, Negative Business Receipt created with timestamp: {}",
        memoFileName,
        businessReceipt.getTimeStamp());

    log.info(ToStringBuilder.reflectionToString(businessReceipt, ToStringStyle.MULTI_LINE_STYLE));
  }
}
