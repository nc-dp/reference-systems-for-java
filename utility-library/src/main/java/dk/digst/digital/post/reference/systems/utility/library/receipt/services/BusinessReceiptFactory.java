package dk.digst.digital.post.reference.systems.utility.library.receipt.services;

import dk.digst.digital.post.memolib.model.MessageHeader;
import dk.digst.digital.post.reference.systems.utility.library.receipt.model.Receipt;
import dk.digst.digital.post.reference.systems.utility.library.receipt.model.ReceiptStatus;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.UUID;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class BusinessReceiptFactory {

  /**
   * If parsing of Memo MessageHeader was successful, creates positive Business Receipt.
   *
   * @param memoMessageHeader - MessageHeader of received MeMo.
   */
  public Receipt createPositiveBusinessReceipt(MessageHeader memoMessageHeader) {

    Receipt businessReceipt = createBusinessReceipt(memoMessageHeader);
    businessReceipt.setReceiptStatus(ReceiptStatus.COMPLETED);
    log.debug("Positive business receipt created {}", businessReceipt.toString());

    return businessReceipt;
  }

  /**
   * If parsing of Memo MessageHeader was unsuccessful, creates negative Business Receipt with
   * relevant error message.
   *
   * @param memoFileName - filename of received MeMo.
   */
  public Receipt createNegativeBusinessReceipt(String memoFileName) {

    Receipt businessReceipt = createBusinessReceipt(null);
    businessReceipt.setErrorMessage(
        String.format("MeMo with uuid: %s could not be parsed.", memoFileName));
    businessReceipt.setReceiptStatus(ReceiptStatus.INVALID);

    return businessReceipt;
  }

  /**
   * Creates a Receipt object with all general fields.
   *
   * @param memoMessageHeader
   * @return receipt
   */
  public Receipt createBusinessReceipt(MessageHeader memoMessageHeader) {

    Receipt receipt = new Receipt();
    if (memoMessageHeader != null) {
      receipt.setMessageUUID(memoMessageHeader.getMessageUUID());
    }
    receipt.setTransmissionId(UUID.randomUUID());
    receipt.setTimeStamp(ZonedDateTime.now(ZoneOffset.UTC).toLocalDateTime());
    return receipt;
  }
}
