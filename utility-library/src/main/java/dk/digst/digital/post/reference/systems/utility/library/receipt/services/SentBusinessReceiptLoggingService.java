package dk.digst.digital.post.reference.systems.utility.library.receipt.services;

import dk.digst.digital.post.reference.systems.utility.library.receipt.model.Receipt;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class SentBusinessReceiptLoggingService {

  /**
   * logs response after sending business receip
   *
   * @param receipt - the sent receipt
   * @param response - response from POST request.
   * @param fileName - name of the file.
   * @return ResponseEntity with HttpStatus.
   */
  public ResponseEntity<String> logSentReceiptResponse(
          Receipt receipt, ResponseEntity<String> response, String fileName) {

    if (response.getStatusCode() == HttpStatus.OK) {
      log.info(
          "Business Receipt with ReceiptStatus: {} for MeMo with uuid: {} sent to NgDP.",
          receipt.getReceiptStatus(),
          fileName);

    } else {
      log.error(
          "Unable to send Business Receipt with ReceiptStatus: {} for MeMo with uuid: {}, httpStatus: {}",
          receipt.getReceiptStatus(),
          fileName,
          response.getStatusCode().toString());
    }

    return response;
  }
}
