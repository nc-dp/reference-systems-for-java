package dk.digst.digital.post.reference.systems.utility.library.memo.services;

import dk.digst.digital.post.memolib.container.MeMoContainerWriter;
import dk.digst.digital.post.memolib.model.Message;
import dk.digst.digital.post.memolib.writer.MeMoStreamWriter;
import dk.digst.digital.post.memolib.xml.writer.MeMoXmlWriterFactory;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.UUID;

@Slf4j
@Service
public class MeMoPersistenceService {

  /**
   * Saves a MeMo as an XML to local file path
   *
   * @param message MeMo to save
   * @return MeMo file
   */
  @SneakyThrows
  public File saveMemo(Message message) {
    File memoFile = createNewFile(getFileName(message.getMessageHeader().getMessageUUID()));

    try (OutputStream stream = new FileOutputStream(memoFile);
        MeMoStreamWriter writer = MeMoXmlWriterFactory.createWriter(stream, true)) {
      writer.write(message);
    }

    return memoFile;
  }

    /**
     * Creates a new file in build/tmp
     * @param filename name of the file
     * @return the file
     */
  @SneakyThrows
  public File createNewFile(String filename){
    File file = new File("build/tmp/" + filename);
    log.info("Creating new file with name {} at {}", file.getName(), file.getAbsolutePath());
    return file;
  }

    /**
     * Writes the message to the writer
     * @param writer Writer to write to
     * @param message Message to write
     */
  @SneakyThrows
  public void writeTarEntry(MeMoContainerWriter writer, Message message){
      writer.writeEntry(getFileName(message.getMessageHeader().getMessageUUID()), message);
  }

  private String getFileName(UUID messageUUID) {
    String fileExtension = ".xml";
    return messageUUID + fileExtension;
  }
}
