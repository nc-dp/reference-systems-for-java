package dk.digst.digital.post.reference.systems.utility.library.receipt.model;

import java.time.LocalDateTime;
import java.util.UUID;
import lombok.Getter;
import lombok.Setter;
import org.springframework.lang.NonNull;

@Getter
@Setter
public class Receipt {

  @NonNull private UUID transmissionId;

  private UUID messageUUID;

  private String errorCode;

  private String errorMessage;

  @NonNull private LocalDateTime timeStamp;

  @NonNull private ReceiptStatus receiptStatus;
}
