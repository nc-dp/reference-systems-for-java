package dk.digst.digital.post.reference.systems.ssl.client;

import dk.digst.digital.post.reference.systems.ssl.client.configurations.EgressRestClientConfigurationProperties;
import dk.digst.digital.post.reference.systems.ssl.client.factories.EgressRestTemplateFactory;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.client.RestTemplate;

@Configuration
@EnableConfigurationProperties(EgressRestClientConfigurationProperties.class)
@ComponentScan
@PropertySource("classpath:/config/egress-rest-client.properties")
public class SslClientConfiguration {

  @Bean
  public RestTemplate sslRestTemplate(EgressRestTemplateFactory egressRestTemplateFactory) {
    return egressRestTemplateFactory.build();
  }
}
