package dk.digst.digital.post.reference.systems.ssl.client.clients;

import dk.digst.digital.post.reference.systems.ssl.client.configurations.EgressRestClientConfigurationProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Slf4j
@Service
public class RestClient {

  @Autowired private RestTemplate sslRestTemplate;

  @Autowired private EgressRestClientConfigurationProperties egressRestClientConfigurationProperties;

  public <T> ResponseEntity<T> post(String uriString, Object requestBody, Class<T> responseClass) {
    log.debug("Sending POST request. uri: {}, body: {}", uriString, requestBody);
    HttpHeaders httpHeaders = createHttpHeaders();

    ResponseEntity<T> response =
        sslRestTemplate.postForEntity(
            uriString, new HttpEntity<>(requestBody, httpHeaders), responseClass);
    log.debug("POST request returned {}", response.toString());
    return response;
  }

  public <T> ResponseEntity<T> sendRequest(
      String uriString, Resource resource, Class<T> responseClass) {
    log.debug("Sending POST request. Uri: {}.", uriString);

    HttpHeaders headers = createHttpHeaders();

    setContentTypeFromUri(uriString, headers);

    ResponseEntity<T> response =
        sslRestTemplate.postForEntity(uriString, new HttpEntity<>(resource, headers), responseClass);
    log.debug("POST request returned {}", response.toString());
    return response;
  }

  // ContentType depends on whether payload is .XML or .TAR
  private void setContentTypeFromUri(String uriString, HttpHeaders headers) {
    if (uriString.contains("memo-message-uuid")) {
      headers.setContentType(MediaType.APPLICATION_XML);
    } else {
      headers.setContentType(MediaType.parseMediaType("application/x-lzma"));
    }
  }

  public <T> ResponseEntity<T> get(
      String uriString, Object requestBody, ParameterizedTypeReference<T> responseType) {
    log.debug("Sending GET request. uri: {}, body: {}", uriString, requestBody);
    HttpHeaders httpHeaders = createHttpHeaders();

    ResponseEntity<T> response =
        sslRestTemplate.exchange(
            uriString, HttpMethod.GET, new HttpEntity<>(httpHeaders), responseType);
    log.debug("GET request returned {}", response.toString());
    return response;
  }

  public <T> ResponseEntity<T> get(String uriString, Class<T> responseType) {
    log.debug("Sending GET request. uri: {}", uriString);
    HttpHeaders httpHeaders = createHttpHeaders();

    ResponseEntity<T> response =
        sslRestTemplate.exchange(
            uriString, HttpMethod.GET, new HttpEntity<>(httpHeaders), responseType);

    log.debug("GET request returned {}", response.toString());
    return response;
  }

  private HttpHeaders createHttpHeaders() {
    String apiKey=
            egressRestClientConfigurationProperties.getSenderSystemApiToken();
    HttpHeaders httpHeaders = new HttpHeaders();
    httpHeaders.setContentType(MediaType.APPLICATION_JSON);
    httpHeaders.setBasicAuth(apiKey);

    return httpHeaders;
  }
}
