package dk.digst.digital.post.reference.systems.ssl.client.factories;

import dk.digst.digital.post.reference.systems.ssl.client.configurations.EgressRestClientConfigurationProperties;
import dk.digst.digital.post.reference.systems.ssl.client.mssl.CompositeTrustStrategy;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.util.Optional;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.http.HttpHost;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContexts;
import org.apache.http.ssl.TrustStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.ResourceLoaderAware;
import org.springframework.context.annotation.Scope;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

@Setter
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@NoArgsConstructor
public class EgressRestTemplateFactory implements ResourceLoaderAware {

  @Autowired
  private EgressRestClientConfigurationProperties egressRestClientConfigurationProperties;

  private ResourceLoader resourceLoader;

  public void setResourceLoader(ResourceLoader resourceLoader) {
    this.resourceLoader = resourceLoader;
  }

  public RestTemplate build() {
    InputStream keystoreInputStream = null;

    try {
      KeyStore keystore = KeyStore.getInstance(KeyStore.getDefaultType());
      keystoreInputStream = getKeystoreInputStream();
      keystore.load(
          keystoreInputStream,
          egressRestClientConfigurationProperties.getKeyStorePassword().toCharArray());

      keystoreInputStream.close();

      SSLContext sslcontext = buildSSLContext(keystore);

      SSLConnectionSocketFactory sslSocketFactory =
          new SSLConnectionSocketFactory(sslcontext, null, null, null);

      Optional<URI> maybeProxy =
          Optional.ofNullable(egressRestClientConfigurationProperties.getProxy())
              .filter(s -> !StringUtils.isEmpty(s))
              .map(URI::create);

      CloseableHttpClient httpClient = buildHttpClient(sslSocketFactory, maybeProxy);

      HttpsURLConnection.setDefaultSSLSocketFactory(sslcontext.getSocketFactory());
      HttpComponentsClientHttpRequestFactory requestFactory =
          new HttpComponentsClientHttpRequestFactory();

      requestFactory.setHttpClient(httpClient);
      requestFactory.setReadTimeout(egressRestClientConfigurationProperties.getReadtimeout());
      requestFactory.setConnectTimeout(egressRestClientConfigurationProperties.getConnecttimeout());

      return new RestTemplate(requestFactory);

    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      if (keystoreInputStream != null) {
        try {
          keystoreInputStream.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }
    return null;
  }

  InputStream getKeystoreInputStream() throws IOException {
    return resourceLoader
        .getResource(egressRestClientConfigurationProperties.getKeyStoreLocation())
        .getInputStream();
  }

  private SSLContext buildSSLContext(KeyStore keystore)
      throws NoSuchAlgorithmException, KeyManagementException, KeyStoreException,
          UnrecoverableKeyException {
    return SSLContexts.custom()
        .setProtocol("TLS")
        .loadKeyMaterial(
            keystore, egressRestClientConfigurationProperties.getKeyStorePassword().toCharArray())
        // null truststore will trigger java to use built in trust store
        .loadTrustMaterial(
            null,
            buildCompositeTrustStrategy(
                Optional.empty(), egressRestClientConfigurationProperties.isTrustselfsigned()))
        .build();
  }

  private TrustStrategy buildCompositeTrustStrategy(
      Optional<TrustStrategy> maybeFirstTrustStrategy, boolean trustSelfSigned) {

    final TrustStrategy firstTrustStrategy = maybeFirstTrustStrategy.orElse(null);
    if (trustSelfSigned) {
      if (firstTrustStrategy != null) {
        return new CompositeTrustStrategy(firstTrustStrategy, new TrustSelfSignedStrategy());
      } else {
        return new TrustSelfSignedStrategy();
      }
    } else {
      return firstTrustStrategy;
    }
  }

  private CloseableHttpClient buildHttpClient(
      SSLConnectionSocketFactory sslSocketFactory, Optional<URI> maybeProxy) {
    return HttpClients.custom()
        .setSSLSocketFactory(sslSocketFactory)
        .setProxy(
            maybeProxy
                .map(proxy -> new HttpHost(proxy.getHost(), proxy.getPort(), proxy.getScheme()))
                .orElse(null))
        .build();
  }
}
