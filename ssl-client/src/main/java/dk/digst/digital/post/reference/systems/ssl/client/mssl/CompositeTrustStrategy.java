package dk.digst.digital.post.reference.systems.ssl.client.mssl;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.List;
import org.apache.http.ssl.TrustStrategy;

public class CompositeTrustStrategy implements TrustStrategy {
    private final List<TrustStrategy> trustStrategyList;

    public CompositeTrustStrategy(TrustStrategy... trustStrategies) {
    this.trustStrategyList = Arrays.asList(trustStrategies);
    }

    @Override
    public boolean isTrusted(X509Certificate[] chain, String authType) throws CertificateException {
        for (var trustStrategy : trustStrategyList) {
            if (trustStrategy.isTrusted(chain, authType)) {
                return true;
            }
        }
        return false;
    }
}
