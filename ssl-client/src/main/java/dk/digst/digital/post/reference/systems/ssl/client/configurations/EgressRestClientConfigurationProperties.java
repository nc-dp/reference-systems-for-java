package dk.digst.digital.post.reference.systems.ssl.client.configurations;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;

@Getter
@Setter
@Validated
@ConfigurationProperties(prefix = "dk.digst.digital.post.egress-rest")
public class EgressRestClientConfigurationProperties {
  @NotBlank private String keyStoreLocation;
  @NotNull private String keyStorePassword = "";
  @NotBlank private String type;
  @NotBlank private String alias;
  private String proxy;
  private boolean trustselfsigned;
  @NotNull private String senderSystemApiToken;

  @Min(10)
  @Max(60000)
  private int readtimeout;

  @Min(10)
  @Max(60000)
  private int connecttimeout;
}
