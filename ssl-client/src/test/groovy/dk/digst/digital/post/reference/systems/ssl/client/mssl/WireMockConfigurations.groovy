package dk.digst.digital.post.reference.systems.ssl.client.mssl


import com.github.tomakehurst.wiremock.core.WireMockConfiguration
import groovy.transform.CompileStatic

@CompileStatic
class WireMockConfigurations {

    private WireMockConfigurations() {}


    static WireMockConfiguration mutualSslWithSelfSignedCerts() {
        return WireMockConfiguration.wireMockConfig()
                .httpDisabled(true)
                .dynamicHttpsPort()
                .trustStoreType('PKCS12')
                .trustStorePassword('')
                .trustStorePath(getClasspathResource('/keystore/server-trust-store.p12'))
                .needClientAuth(true)
                .keystorePath(getClasspathResource('/keystore/server-private-key.pfx'))
                .keystorePassword('')
                .keyManagerPassword('')
    }

    static WireMockConfiguration mutualSslWithValidVocesCert() {
        return vocesCert('/keystore/VOCES_valid_2022.p12')
    }

    private static WireMockConfiguration vocesCert(String privateKeyPath) {
        return WireMockConfiguration.wireMockConfig()
                .httpDisabled(true)
                .dynamicHttpsPort()
                .trustStoreType('PKCS12')
                .trustStorePassword('')
                .trustStorePath(getClasspathResource('/keystore/server-trust-store.p12'))
                .needClientAuth(true)
                .keystorePath(getClasspathResource(privateKeyPath))
                .keystorePassword('Test1234')
                .keyManagerPassword('Test1234')
    }

    private static String getClasspathResource(String privateKeyPath) {
        return WireMockConfigurations.getResource(privateKeyPath).toString()
    }
}
