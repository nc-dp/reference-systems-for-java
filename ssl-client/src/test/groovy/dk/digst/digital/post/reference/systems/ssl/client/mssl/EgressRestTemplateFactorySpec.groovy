package dk.digst.digital.post.reference.systems.ssl.client.mssl


import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.junit.WireMockClassRule
import org.apache.http.conn.HttpHostConnectException
import org.junit.ClassRule
import org.springframework.web.client.ResourceAccessException
import spock.lang.Shared

import java.security.cert.CertPathBuilderException

class EgressRestTemplateFactorySpec extends AbstractSpec {

  @ClassRule
  @Shared
  WireMockClassRule wireMockServer = new WireMockClassRule(WireMockConfigurations.mutualSslWithSelfSignedCerts())

  void 'Fail on incorrect proxy url'() {
    given:
    def restTemplate = setupEgressRestTemplateFactory {
      selfSignedCerts(it)
      it.proxy = 'http://localhost:1234'
    }.build()

    when:
    callDummyEndpoint(restTemplate, wireMockServer)

    then:
    ResourceAccessException e = thrown(ResourceAccessException)
    e.cause instanceof HttpHostConnectException
    e.cause.message.contains('localhost:1234')
  }

  void 'Reject valid VOCES server cert if no CA'() {
    given:
    WireMockServer vocesMockServer = new WireMockServer(WireMockConfigurations.mutualSslWithValidVocesCert())
    vocesMockServer.start()

    and:
    def restTemplate = setupEgressRestTemplateFactory { selfSignedCerts(it) }.build()

    when:
    callDummyEndpoint(restTemplate, vocesMockServer)

    then:
    ResourceAccessException e = thrown(ResourceAccessException)
    e.rootCause instanceof CertPathBuilderException

    cleanup:
    vocesMockServer.stop()
  }
}
