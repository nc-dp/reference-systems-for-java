package dk.digst.digital.post.reference.systems.ssl.client.mssl

import com.github.tomakehurst.wiremock.WireMockServer
import dk.digst.digital.post.reference.systems.ssl.client.configurations.EgressRestClientConfigurationProperties
import dk.digst.digital.post.reference.systems.ssl.client.factories.EgressRestTemplateFactory
import groovy.transform.CompileStatic
import org.springframework.core.io.ClassRelativeResourceLoader
import org.springframework.http.ResponseEntity
import org.springframework.web.client.RestTemplate
import spock.lang.Specification

@CompileStatic
abstract class AbstractSpec extends Specification {

  protected ResponseEntity<String> callDummyEndpoint(RestTemplate restTemplate, WireMockServer wireMockServer) {
    return restTemplate.getForEntity("https://localhost:${wireMockServer.httpsPort()}/dummy-endpoint", String)
  }

  protected EgressRestTemplateFactory setupEgressRestTemplateFactory(Closure propertiesCustomizer = Closure.IDENTITY) {
    def egressRestClientConfigurationProperties = new EgressRestClientConfigurationProperties()
    propertiesCustomizer.call(egressRestClientConfigurationProperties)
    EgressRestTemplateFactory egressRestTemplateFactory = new EgressRestTemplateFactory()
    egressRestTemplateFactory.resourceLoader = new ClassRelativeResourceLoader(this.getClass())
    egressRestTemplateFactory.setEgressRestClientConfigurationProperties(egressRestClientConfigurationProperties)
    return egressRestTemplateFactory
  }

  static void selfSignedCerts(EgressRestClientConfigurationProperties properties) {
    properties.alias = 'client-private-key'
    properties.keyStoreLocation = 'classpath:/keystore/client-private-key.pfx'
    properties.trustselfsigned = true
  }
}
