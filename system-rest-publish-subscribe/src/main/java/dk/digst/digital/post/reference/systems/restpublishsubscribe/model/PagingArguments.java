package dk.digst.digital.post.reference.systems.restpublishsubscribe.model;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.domain.Pageable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PagingArguments {

  private int page;

  private int size;

  private List<Order> ordering;

  public PagingArguments(int page, int size) {
    this(page, size, new ArrayList<>());
  }

  public PagingArguments(Pageable pageable) {
    this(
        pageable.getPageNumber(),
        pageable.getPageSize(),
        pageable
            .getSort()
            .stream()
            .map(
                order ->
                    new Order(
                        order.getProperty(), Direction.valueOf(order.getDirection().toString())))
            .collect(Collectors.toList()));
  }

  @AllArgsConstructor(access = AccessLevel.PRIVATE)
  public enum Direction {
    ASC,
    DESC;
  }

  @Getter
  @Setter
  @NoArgsConstructor
  @AllArgsConstructor
  public static class Order {

    private String property;

    private Direction direction;

    public Order(String property) {
      this(property, Direction.ASC);
    }

    @Override
    public String toString() {
      return String.format("%s,%s", property, direction);
    }
  }
}
