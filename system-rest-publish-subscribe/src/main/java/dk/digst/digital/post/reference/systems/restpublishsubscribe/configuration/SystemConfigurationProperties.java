package dk.digst.digital.post.reference.systems.restpublishsubscribe.configuration;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Getter
@Setter
@Component
@ConfigurationProperties(prefix = "dk.digst.digital.post.systems")
public class SystemConfigurationProperties {
    private String ngdpEndpoint;
    private String ngdpReceiptEndpoint;
}
