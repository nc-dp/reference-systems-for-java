package dk.digst.digital.post.reference.systems.restpublishsubscribe;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan(basePackages = "dk.digst.digital.post.reference.systems")
public class ReferenceSystemsRestPublishSubscribeConfiguration {}
