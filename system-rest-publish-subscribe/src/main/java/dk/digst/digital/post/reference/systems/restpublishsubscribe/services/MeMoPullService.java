package dk.digst.digital.post.reference.systems.restpublishsubscribe.services;

import dk.digst.digital.post.memolib.model.MessageHeader;
import dk.digst.digital.post.memolib.parser.MeMoParseException;
import dk.digst.digital.post.reference.systems.restpublishsubscribe.configuration.SystemConfigurationProperties;
import dk.digst.digital.post.reference.systems.restpublishsubscribe.model.PagingArguments;
import dk.digst.digital.post.reference.systems.restpublishsubscribe.model.PagingResult;
import dk.digst.digital.post.reference.systems.ssl.client.clients.RestClient;
import dk.digst.digital.post.reference.systems.utility.library.memo.services.MeMoLoggingService;
import dk.digst.digital.post.reference.systems.utility.library.memo.services.MeMoParsingService;
import dk.digst.digital.post.reference.systems.utility.library.receipt.model.Receipt;
import dk.digst.digital.post.reference.systems.utility.library.receipt.services.BusinessReceiptFactory;
import dk.digst.digital.post.reference.systems.utility.library.receipt.services.SentBusinessReceiptLoggingService;
import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class MeMoPullService {

  @Autowired RestClient restClient;

  @Autowired private MeMoParsingService meMoParsingService;

  @Autowired private BusinessReceiptFactory businessReceiptFactory;

  @Autowired private MeMoLoggingService memoLoggingService;

  @Autowired private SentBusinessReceiptLoggingService sentBusinessReceiptLoggingService;

  @Autowired private SystemConfigurationProperties systemConfigurationProperties;

  /*
   * <p>Recipient-systems with the REST_PUBLISH_SUBSCRIBE protocol interact with NgDP by getting a list of all
   * available MeMos that are ready for fetching. This allows the Recipient-System to fetch MeMos
   * whenever configured to do so, instead of receiving them automatically whenever they are
   * processed by NgDP - as is the case of REST_PUSH.
   *
   * <p>This method executes the three primary steps of the REST_PULL interaction with NgDP
   *  1. Getting a list of all MeMos available for fetching by the Recipient-system
   *  2. Fetching each of these MeMos
   *  3. Sending a Business Receipt for each MeMo back to NgDP.
   */
  public void pullAvailableMeMos() throws IOException {

    // PagingArguments, requesting page 1 with a max of 10 entities.
    PagingArguments pagingArguments = new PagingArguments(1, 10);
    String pagingParameters =
            String.format("?page=%s&size=%s", pagingArguments.getPage(), pagingArguments.getSize());

    // NgDP REST endpoint (on test01) for getting a list of available memos.
    String availableMemoListEndpoint =
            systemConfigurationProperties.getNgdpEndpoint() + pagingParameters;

    // Sending the request to the endpoint.
    ResponseEntity<PagingResult<UUID>> fetchMemoResponse;
    try {
      fetchMemoResponse = restClient.get(
              availableMemoListEndpoint,
              pagingArguments,
              new ParameterizedTypeReference<PagingResult<UUID>>() {});
    } catch (Exception e) {
      log.error("An error occurred during fetching list of MeMos: {}", e.toString());
      return;
    }

    if (!responseIsNotNull(fetchMemoResponse)) return;

    List<UUID> listOfAvailableMemos =
        Objects.requireNonNull(fetchMemoResponse.getBody()).getContent();

    // For each uniquely identified MeMo in the list of available MeMemos:
    // 1. Get the MeMo from NgDP.
    for (UUID memoUuid : listOfAvailableMemos) {
      ResponseEntity<Resource> fetchResult =
          restClient.get(systemConfigurationProperties.getNgdpEndpoint() + memoUuid.toString(), Resource.class);

      if (fetchResult.getStatusCode() == HttpStatus.OK && fetchResult.getBody() != null) {
        log.info("MeMo with UUID: {} successfully fetched from NgDP.", memoUuid);

      } else {
        log.error(
            "Was not able to fetch MeMo with UUID: {}. HttpStatusCode: {}:",
            memoUuid,
            fetchResult.getStatusCode());
        continue;
      }
      processPulledMeMo(fetchResult);
    }
  }

  public void processPulledMeMo(ResponseEntity<Resource> fetchResult) throws IOException {
    Resource memoFile = fetchResult.getBody();
    String memoFileName = memoFile.getFilename();
    MessageHeader memoMessageHeader = new MessageHeader();
    Receipt receipt;

    try {
      // Assess the validity of the MeMo MessageHeader and produce Business Receipt.
      memoMessageHeader = meMoParsingService.parseMemo(memoFile.getInputStream());
      receipt = businessReceiptFactory.createPositiveBusinessReceipt(memoMessageHeader);
      memoLoggingService.logMemoSuccessfullyParsed(receipt, memoMessageHeader);
    } catch (MeMoParseException e) {
      receipt = businessReceiptFactory.createNegativeBusinessReceipt(memoFileName);
      memoLoggingService.logMemoUnsuccessfullyParsed(receipt, memoFileName);
    }

    final String ngdpRestPostReceiptEndpoint =
        String.format(
            systemConfigurationProperties.getNgdpReceiptEndpoint(), memoFileName);

    ResponseEntity<String> response =
        restClient.post(ngdpRestPostReceiptEndpoint, receipt, String.class);

    sentBusinessReceiptLoggingService.logSentReceiptResponse(
        receipt, response, memoFileName);
  }

  private boolean responseIsNotNull(ResponseEntity<PagingResult<UUID>> fetchMemoResponse) {
    if (fetchMemoResponse != null
        && fetchMemoResponse.getBody() != null
        && fetchMemoResponse.getBody().getContent() != null) {
      log.info(
          "Successfully fetched information of {} memos ready for delivery.",
          fetchMemoResponse.getBody().getSize());
    } else {
      log.error("Unable to process response from NgDP.");
      return false;
    }
    return true;
  }
}
