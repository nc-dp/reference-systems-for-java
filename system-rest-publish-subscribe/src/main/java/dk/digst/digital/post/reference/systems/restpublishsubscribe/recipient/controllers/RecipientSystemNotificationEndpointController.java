package dk.digst.digital.post.reference.systems.restpublishsubscribe.recipient.controllers;

import dk.digst.digital.post.reference.systems.ssl.client.clients.RestClient;
import dk.digst.digital.post.reference.systems.restpublishsubscribe.model.PublishSubscribeNotificationCommand;
import dk.digst.digital.post.reference.systems.restpublishsubscribe.recipient.api.RecipientSystemNotificationEndpointApi;
import dk.digst.digital.post.reference.systems.restpublishsubscribe.services.MeMoPullService;
import java.io.IOException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class RecipientSystemNotificationEndpointController
    implements RecipientSystemNotificationEndpointApi {

  @Autowired private MeMoPullService meMoPullService;

  @Autowired private RestClient restClient;

  /**
   * REST-method that simulates the endpoint of a REST_PUBLISH_SUBSCRIBE recipient-system configured through
   * Administrative access. When NgDP has validated a MeMo, it will look up the protocol of the
   * recipient-system, and in case of REST_PULL, immediately send a notification to the
   * recipient-system containing a link to the MeMo.
   *
   * @param publishSubscribeNotificationCommand - object containing the URL for the MeMo available for fetching.
   */
  public ResponseEntity<Void> recipientSystemNotificationEndpoint(
      PublishSubscribeNotificationCommand publishSubscribeNotificationCommand) throws IOException {

    String memoUrl = publishSubscribeNotificationCommand.getUrl();
    log.info("Trying to fetch MeMo from {}", memoUrl);

    ResponseEntity<Resource> fetchResult;
    try {
      fetchResult = restClient.get(memoUrl, Resource.class);
    } catch (Exception e) {
      log.error("An error occurred during fetching of MeMo: {}", e.toString());
      return null;
    }

    meMoPullService.processPulledMeMo(fetchResult);

    return new ResponseEntity<>(HttpStatus.OK);
  }
}
