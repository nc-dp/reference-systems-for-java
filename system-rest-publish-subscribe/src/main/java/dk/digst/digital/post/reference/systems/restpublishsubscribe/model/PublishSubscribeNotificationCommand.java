package dk.digst.digital.post.reference.systems.restpublishsubscribe.model;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class PublishSubscribeNotificationCommand {
  String url;
}
