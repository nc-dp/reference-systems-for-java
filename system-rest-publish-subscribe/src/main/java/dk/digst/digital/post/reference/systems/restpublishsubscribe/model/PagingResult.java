package dk.digst.digital.post.reference.systems.restpublishsubscribe.model;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Slice;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class PagingResult<T> {

  private List<T> content;

  private int number;

  private int size;

  private PagingArguments previous;

  private PagingArguments next;

  private Long totalElements;

  private Integer totalPages;

  public PagingResult(Slice<T> slice) {
    this(
        slice.getContent(),
        slice.getNumber(),
        slice.getSize(),
        slice.hasPrevious() ? new PagingArguments(slice.previousPageable()) : null,
        slice.hasNext() ? new PagingArguments(slice.nextPageable()) : null);
  }

  public PagingResult(Page<T> page) {
    this(
        page.getContent(),
        page.getNumber(),
        page.getSize(),
        page.hasPrevious() ? new PagingArguments(page.previousPageable()) : null,
        page.hasNext() ? new PagingArguments(page.nextPageable()) : null,
        page.getTotalElements(),
        page.getTotalPages());
  }

  public PagingResult(
      List<T> content, int number, int size, PagingArguments previous, PagingArguments next) {
    this(content, number, size, previous, next, null, null);
  }

  public <R> PagingResult<R> map(Function<? super T, ? extends R> converter) {
    return new PagingResult<>(
        content.stream().map(converter::apply).collect(Collectors.toList()),
        number,
        size,
        previous,
        next,
        totalElements,
        totalPages);
  }
}
