package dk.digst.digital.post.reference.systems.restpublishsubscribe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ReferenceSystemsRestPublishSubscribeApplication {

  public static void main(String[] args) {
    SpringApplication.run(ReferenceSystemsRestPublishSubscribeApplication.class, args);
  }
}
