package dk.digst.digital.post.reference.systems.restpublishsubscribe.recipient.api;

import dk.digst.digital.post.memolib.parser.MeMoParseException;
import dk.digst.digital.post.reference.systems.restpublishsubscribe.model.PublishSubscribeNotificationCommand;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import java.io.IOException;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping(value = "notification")
public interface RecipientSystemNotificationEndpointApi {

  @PostMapping(
      value = "/",
      consumes = {MediaType.APPLICATION_JSON_VALUE},
      produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
  @Operation(
      description = "Receives notifications for MeMos available for fetching",
      responses = {
        @ApiResponse(responseCode = "200", description = "OK"),
        @ApiResponse(responseCode = "400", description = "Bad request"),
        @ApiResponse(responseCode = "401", description = "Unauthorized"),
        @ApiResponse(responseCode = "403", description = "Forbidden"),
        @ApiResponse(responseCode = "404", description = "Not found"),
        @ApiResponse(responseCode = "503", description = "Service unavailable"),
      })
  ResponseEntity<Void> recipientSystemNotificationEndpoint(@RequestBody PublishSubscribeNotificationCommand command)
      throws MeMoParseException, IOException;
}
