package dk.digst.digital.post.reference.systems.restpublishsubscribe.startup;

import dk.digst.digital.post.reference.systems.restpublishsubscribe.services.MeMoPullService;
import java.io.IOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class SystemRestPullStartupRunner implements CommandLineRunner {

  @Autowired private MeMoPullService memoPullService;

  @Override
  public void run(String... args) throws IOException {
    memoPullService.pullAvailableMeMos();
  }
}
